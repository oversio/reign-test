import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles'

import Header from './components/Header'
import ListHN from './components/List'

const BASE_URL_API = 'http://localhost:8080'

export default class App extends Component { 
  constructor() {
    super();

    this.state = {
      hits: [],
    }
  }

  useStyles() {
    return makeStyles(theme => ({
      container: {
        flexGrow: 1,
      },
    }));
  }

  handleDelete(row) {
    const data = { objectID: row._id }
    fetch(`${ BASE_URL_API }/hackers/news/delete`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .catch(error => console.log('error->', error))
    .then(res => {
      if (res.status) {
        this.componentDidMount()
      }
    })
  }

  handleSelect(item) {
    window.open(item.story_url || item.url)
  }

  componentDidMount() {
    fetch(`${ BASE_URL_API }/hackers/news`)
      .then(response => response.json())
      .then(data => this.setState({ hits: data.hits }));
  }

  render() {
    const classes = this.useStyles();

    return (
      <Box className={classes.container}>
        <Grid container>
          <Grid item xs={12}>
            <Header />
          </Grid>
          <Grid item xs={12}>
            <ListHN 
            items={ this.state.hits } 
            onSelect={ this.handleSelect.bind(this) }
            onDelete={ this.handleDelete.bind(this) } />
          </Grid>
        </Grid>
      </Box>
    )
  }
}