import React from 'react'
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles'
import DeleteIcon from '@material-ui/icons/Delete'
import * as moment from 'moment'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles(theme => ({
	container: {
		padding: '30px',
	},
	table: {
		minWidth: 650,
	},
	delete: {
		cursor: 'pointer',
	},
	tableRow: {
		cursor: 'pointer',
		backgroundColor: '#fff',
		'&:hover': {
			background: "#fafafa",
		},
	},
	tableCell: {
		borderBottom: '1px solid #ccc',
		color: '#333',
		fontSize: '13px',
	},
	author: {
		color: '#999',
	}
}))

export default function ListHN(props) {
	const classes = useStyles();
	const { items, onDelete, onSelect } = props;
	const rows = items.filter(i => {
		return (i.story_title || i.title) && i.is_active
	})

	const onDeleteItem = (row) => {
		onDelete(row)
	}

	const handleClickRow = (row) => {
		onSelect(row)
	}

	const getDateFormat = (date) => {
		return moment(date).calendar(null, {
			sameDay: 'h:mm a',
			nextDay: 'MMM DD',
			nextWeek: 'MMM DD',
			lastDay: '[Yesterday]',
			lastWeek: 'MMM DD',
			sameElse: 'MMM DD',
		});
	}

	return (
		
		<Box className={ classes.container }>
			<TableContainer>
				<Table className={classes.table} aria-label="simple table">
					<TableBody>
						{rows.map((row, key) => (
							<TableRow 
							key={ key } 
							className={ classes.tableRow }>
								<TableCell component="th" scope="row" className={ classes.tableCell } onClick={ handleClickRow.bind(this, row) }>
									{ row.story_title || row.title }
									<span className={ classes.author }> - { row.author } -</span>
								</TableCell>
								<TableCell align="right" className={ classes.tableCell } onClick={ handleClickRow.bind(this, row) }>
									{ getDateFormat(row.created_at) }
								</TableCell>
								<TableCell align="right" className={ classes.tableCell }>
									<DeleteIcon 
									className={ classes.delete }
									onClick={ onDeleteItem.bind(this, row) } />
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			
		</Box>
	)
}
