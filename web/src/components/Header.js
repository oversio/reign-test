import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
	header: {
		display: 'flex',
		height: '35vh',
		alignItems: 'center',
		paddingLeft: '3%',
	},
	content: {
		display: 'flex',
		justifyContent: 'flex-start',
		flexDirection: 'column',
	},
	h1: { 
		marginBottom: 0,
		fontSize: '4em',
	},
	p: { 
		marginTop: '3px',
		fontSize: '1.5em',
	},
}))

export default function Header() {
	const classes = useStyles();
	const slogan = 'We <3 hacker news!'
	
	return (
		<Box bgcolor="primary.main" color="primary.contrastText" className={ classes.header }>
			<div className={ classes.content }>
				<h1 className={ classes.h1 }>HN Feed</h1>
				<p className={ classes.p }>{slogan}</p>
			</div>
		</Box>
	)
}

