[![pipeline status](https://gitlab.com/oversio/reign-test/badges/master/pipeline.svg)](https://gitlab.com/oversio/reign-test/-/commits/master)
[![coverage report](https://gitlab.com/oversio/reign-test/badges/master/coverage.svg)](https://gitlab.com/oversio/reign-test/-/commits/master)

# We <3 hackers news

## Requiments
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/)

## Steps
* Clone this repo
* Enter to the folder ```cd reign-test```
* In the root of folder run ``` docker-compose up --build ```
* Wait for the process to finish.
* Go to [http://localhost:3000](http://localhost:3000)
* That's it!.
