//Load app dependencies  
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const errorHandler = require('errorhandler')
const http = require('http')
const port = 8080
const cors = require('cors')
const app = express();
const services = require('./routes/router')
const test = process.env.NODE_ENV === 'test'

mongoose.connect(process.env.MONGO_URL, { 
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
})

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride());


app.get('/hackers/news', services.getHackerNews)
app.post('/hackers/news/delete', services.deleteHackerNews)
app.post('/hackers/populate', services.checkExternalInfo)

if (!test) {
	services.checkExternalInfo()
}

app.use(errorHandler());

const server = http.createServer(app)

server.listen(port, () => {
	console.log(`Express server listening on port ${port}`);
})

module.exports = app