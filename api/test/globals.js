const api = require('../app.js')
const chai = require('chai')
const chaiHttp = require('chai-http')

chai.use(chaiHttp)

global._api = api
global._chai = chai