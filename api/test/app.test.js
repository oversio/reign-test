describe('Testing App', () => {
	let toDelete = {}

	describe('Populating DB', () => {
		it('Populating HN list', (done) => {
			_chai.request(_api)
				.post('/hackers/populate')
				.then((response) => {
					_chai.should().exist(response)
					response.should.have.status(200)
					_chai.assert(response.body.status === true, 'Status should be true')

					done()
				})
				.catch(done)
		});
	});

	describe('Getting list', () => {
		it('Get HN list', (done) => {
			_chai.request(_api)
				.get('/hackers/news')
				.then((response) => {
					_chai.should().exist(response)
					response.should.have.status(200)
					response.body.hits.should.be.a('array')

					toDelete = response.body.hits[0]
					done()
				})
				.catch(done)
		});
	});

	describe('Deleting element', () => {
		it('Delete first element', (done) => {
			_chai.request(_api)
				.post('/hackers/news/delete')
				.send({ objectID: toDelete._id })
				.then((response) => {
					_chai.should().exist(response)
					response.should.have.status(200)
					_chai.assert(response.body.status === true, 'Status should be true')
					_chai.assert(response.body.message === 'success!', 'Message is changed')
					response.body.should.have.property('hit')
					response.body.hit.should.be.a('object')
					done()
				})
				.catch(done)
		});
	});
});