const mongoose = require('mongoose')
const Schema = mongoose.Schema

const hackerNews = new Schema({
	is_active: { type: Boolean, default: true },
	created_at: String,
	title: String,
	url: String,
	author: String,
	points: String,
	story_text: String,
	comment_text: String,
	num_comments: String,
	story_id: String,
	story_title: String,
	story_url: String,
	parent_id: String,
	created_at_i: String,
	_tags: Object,
	objectID: String,
	_highlightResult: Object,
})

try {
	module.exports = mongoose.model('HackerNews')
} catch (e) {
	module.exports = mongoose.model('HackerNews', hackerNews)
}
