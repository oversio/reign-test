const express = require('express')

const request = require('request')

const externalUrl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
const HackerNews = require('../models/hackerNews')

const getHackerNews = (_req, res) => {
	HackerNews
		.find({})
		.sort({ created_at: -1 })
		.exec((_err, hits) => res.json({ hits }))
}

const checkExternalInfo = (req, res) => {
	request(
		{ uri: externalUrl },
		(_err, _response, body) => {
			const { hits } = JSON.parse(body)

			hits.map((item) => {
				HackerNews.findOne(
					{ objectID: item.objectID },
					(_err, record) => {
						if (!record) {
							HackerNews.create(item)
						}
					}
				)
			})

			if (process.env.NODE_ENV !== 'test') {
				setTimeout(() => {
					checkExternalInfo()
				}, 60000 * 60);
			} else {
				res.json({ status: (hits.length >= 0) })
			}

		}
	)
	
}

const deleteHackerNews = (req, res) => {
	const { objectID } = req.body

	if (objectID) {
		HackerNews.findByIdAndUpdate(objectID, { is_active: false }, (err, result) => {
			if (err) {
				res.json({ status: false, message: err })
				
				return
			}

			res.json({ 
				status: true, 
				message: 'success!', 
				hit: result,
			})
		});
	} else {
		res.json({ status: false, message: 'objectID is not found in request'})
	}

}

module.exports = {
	getHackerNews,
	checkExternalInfo,
	deleteHackerNews,
}